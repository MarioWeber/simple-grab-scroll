var sgsModkey = 0;

let startMouseX = 0.0;
let startMouseY = 0.0;
let startScrollX = 0.0;
let startScrollY = 0.0;

// if the user grab scrolls with the cursor on top  of a link
// don't open the link in a new tab
let preventNextDefault = false;

function scroll(e) {
    let x = startScrollX + startMouseX - e.clientX;
    let y = startScrollY + startMouseY - e.clientY;
    window.scrollTo(x, y);
    preventNextDefault = true;
}

function mupHandler(e) {
    document.body.style.cursor = "auto";
    window.removeEventListener("mousemove", scroll, true);
}

function eventCancel(e) {
    if (preventNextDefault) {
        preventNextDefault = false;
        e.preventDefault();
    }
    window.removeEventListener(e.type, eventCancel, true);
}

window.addEventListener("mousedown", function(e){
    if (e.button != 1) { return; }
    if (sgsModkey == 0 && (e.shiftKey || e.ctrlKey || e.altKey)) { return; }
    if ((sgsModkey & 1) && !e.shiftKey) { return; }
    if ((sgsModkey & 2) && !e.ctrlKey) { return; }
    if ((sgsModkey & 4) && !e.altKey) { return; }
    e.preventDefault();
    document.body.style.cursor = "grabbing";
    startMouseX = e.clientX;
    startMouseY = e.clientY;
    startScrollX = window.scrollX;
    startScrollY = window.scrollY;
    window.addEventListener("mousemove", scroll, true);
    window.addEventListener("mouseup", mupHandler, true);
    window.addEventListener("click", eventCancel, true);
    window.addEventListener("wheel", eventCancel, true);
}, true);

browser.runtime.onMessage.addListener((opts) => {
    sgsModkey = opts.modkey;
});

(function(){
    browser.storage.local.get({"modkey":0}, vals => {
        sgsModkey = vals.modkey;
    });
})();
