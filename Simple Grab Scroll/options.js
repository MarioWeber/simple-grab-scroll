function changeHandler(e) {
    var n = e.target.getAttribute("name");
    if (n != "modkey") {
        return;
    }
    var mod=0;
    if (document.getElementById("m1").checked) { mod += 1; }
    if (document.getElementById("m2").checked) { mod += 2; }
    if (document.getElementById("m3").checked) { mod += 4; }

    browser.storage.local.set({"modkey":mod});
    browser.tabs.query({currentWindow: true}, tabs => {
        for (let t of tabs) {
            if (!t.url.startsWith("about:")) {
                browser.tabs.sendMessage(t.id, {"modkey":mod});
            }
        }
    });
}

document.addEventListener('DOMContentLoaded', function(){
	browser.storage.local.get({"modkey":0}, vals => {
        if (vals.modkey & 1) {
            document.getElementById("m1").checked = true;
        }
        if (vals.modkey & 2) {
            document.getElementById("m2").checked = true;
        }
        if (vals.modkey & 4) {
            document.getElementById("m3").checked = true;
        }
    });

    for (let i of ["m1","m2","m3"]) {
        document.getElementById(i).onchange = changeHandler;
    }
});